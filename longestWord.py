#           Costa Rica Institute of Technology.
#           Developed by Cristian Alvarado Gómez    cristianmiguelag@gmail.com
#           Technical test Avantica San Carlos
import re   #Library to use regular expression https://docs.python.org/3.1/library/re.html
def longestWord(sen):
    if sen: #if sen value is empty do not process the following segment of code and continue with else.
        words = re.findall(r"[a-zA-Zá-úÁ-Ú]+", sen) #Save words that match the regular expression in the variable "words"
        tempWord =""
        for i in words:                 #
            if len(i) > len(tempWord):  #Compare and select the longest word and save it in the tempWord variable.
                tempWord= i             #
        print("The longest word is:",tempWord)
    else:
        print("Insert a string value")


longestWord("fun&!!time")
longestWord("I Love dogs")
longestWord("")